import * as mocha from "mocha";
import { expect } from "chai";
import {Headers} from "../src/headers";

export function main() {
  describe("Headers", () => {

    describe("initialization", () => {
      it("should conform to spec", () => {
        const httpHeaders = {
          "Content-Type": "image/jpeg",
          "Accept-Charset": "utf-8",
          "X-My-Custom-Header": "Zeke are cool",
        };
        const secondHeaders = new Headers(httpHeaders);
        const secondHeadersObj = new Headers(secondHeaders);
        expect(secondHeadersObj.get("Content-Type")).to.be.equal("image/jpeg");
      });

      it("should merge values in provided dictionary", () => {
        const headers = new Headers({"foo": "bar"});
        expect(headers.get("foo")).to.be.equal("bar");
        expect(headers.getAll("foo")).to.be.equal(["bar"]);
      });

      it("should not alter the values of a provided header template", () => {
        // Spec at https://fetch.spec.whatwg.org/#concept-headers-fill
        // test for https://github.com/angular/angular/issues/6845
        const firstHeaders = new Headers();
        const secondHeaders = new Headers(firstHeaders);
        secondHeaders.append("Content-Type", "image/jpeg");
        expect(firstHeaders.has("Content-Type")).to.be.equal(false);
      });

      it("should preserve the list of values", () => {
        const src = new Headers();
        src.append("foo", "a");
        src.append("foo", "b");
        src.append("foo", "c");
        const dst = new Headers(src);
        expect(dst.getAll("foo")).to.be.equal(src.getAll("foo"));
      });

      it("should keep the last value when initialized from an object", () => {
        const headers = new Headers({
          "foo": "first",
          "fOo": "second",
        });

        expect(headers.getAll("foo")).to.be.equal(["second"]);
      });
    });

    describe(".set()", () => {
      it("should clear all values and re-set for the provided key", () => {
        const headers = new Headers({"foo": "bar"});
        expect(headers.get("foo")).to.be.equal("bar");

        headers.set("foo", "baz");
        expect(headers.get("foo")).to.be.equal("baz");

        headers.set("fOO", "bat");
        expect(headers.get("foo")).to.be.equal("bat");
      });

      it("should preserve the case of the first call", () => {
        const headers = new Headers();
        headers.set("fOo", "baz");
        headers.set("foo", "bat");
        expect(JSON.stringify(headers)).to.be.equal("{'fOo':['bat']}");
      });

      it("should convert input array to string", () => {
        const headers = new Headers();
        headers.set("foo", ["bar", "baz"]);
        expect(headers.get("foo")).to.be.equal("bar,baz");
        expect(headers.getAll("foo")).to.be.equal(["bar,baz"]);
      });
    });

    describe(".get()", () => {
      it("should be case insensitive", () => {
        const headers = new Headers();
        headers.set("foo", "baz");
        expect(headers.get("foo")).to.be.equal("baz");
        expect(headers.get("FOO")).to.be.equal("baz");
      });

      it("should return null if the header is not present", () => {
        const headers = new Headers({bar: []});
        expect(headers.get("bar")).to.be.equal(null);
        expect(headers.get("foo")).to.be.equal(null);
      });
    });

    describe(".getAll()", () => {
      it("should be case insensitive", () => {
        const headers = new Headers({foo: ["bar", "baz"]});
        expect(headers.getAll("foo")).to.be.equal(["bar", "baz"]);
        expect(headers.getAll("FOO")).to.be.equal(["bar", "baz"]);
      });

      it("should return null if the header is not present", () => {
        const headers = new Headers();
        expect(headers.getAll("foo")).to.be.equal(null);
      });
    });

    describe(".delete", () => {
      it("should be case insensitive", () => {
        const headers = new Headers();

        headers.set("foo", "baz");
        expect(headers.has("foo")).to.be.equal(true);
        headers.delete("foo");
        expect(headers.has("foo")).to.be.equal(false);

        headers.set("foo", "baz");
        expect(headers.has("foo")).to.be.equal(true);
        headers.delete("FOO");
        expect(headers.has("foo")).to.be.equal(false);
      });
    });

    describe(".append", () => {
      it("should append a value to the list", () => {
        const headers = new Headers();
        headers.append("foo", "bar");
        headers.append("foo", "baz");
        expect(headers.get("foo")).to.be.equal("bar");
        expect(headers.getAll("foo")).to.be.equal(["bar", "baz"]);
      });

      it("should preserve the case of the first call", () => {
        const headers = new Headers();

        headers.append("FOO", "bar");
        headers.append("foo", "baz");
        expect(JSON.stringify(headers)).to.be.equal("{"FOO":["bar","baz"]}");
      });
    });

    describe(".toJSON()", () => {
      let headers: Headers;
      let values: string[];
      let ref: {[name: string]: string[]};

      beforeEach(() => {
        values = ["application/jeisen", "application/jason", "application/patrickjs"];
        headers = new Headers();
        headers.set("Accept", values);
        ref = {"Accept": values};
      });

      it("should be serializable with toJSON",
         () => { expect(JSON.stringify(headers)).to.be.equal(JSON.stringify(ref)); });

      it("should be able to recreate serializedHeaders", () => {
        const parsedHeaders = JSON.parse(JSON.stringify(headers));
        const recreatedHeaders = new Headers(parsedHeaders);
        expect(JSON.stringify(parsedHeaders)).to.be.equal(JSON.stringify(recreatedHeaders));
      });
    });

    describe(".fromResponseHeaderString()", () => {
      it("should parse a response header string", () => {
        const response = `Date: Fri, 20 Nov 2015 01:45:26 GMT\n` +
            `Content-Type: application/json; charset=utf-8\n` +
            `Transfer-Encoding: chunked\n` +
            `Connection: keep-alive`;
        const headers = Headers.fromResponseHeaderString(response);
        expect(headers.get("Date")).to.be.equal("Fri, 20 Nov 2015 01:45:26 GMT");
        expect(headers.get("Content-Type")).to.be.equal("application/json; charset=utf-8");
        expect(headers.get("Transfer-Encoding")).to.be.equal("chunked");
        expect(headers.get("Connection")).to.be.equal("keep-alive");
      });
    });
  });
}
