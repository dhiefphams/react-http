import { request as NodeRequest, RequestOptions as NodeRequestOpts, ClientResponse as NodeResponse } from "http";
import { isNull, assign } from "lodash";
import { Url, parse } from "url";
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { ConnectionBackend, Connection } from "../interfaces";
import { Request } from "../static_request";
import { Response } from "../static_response";
import { ReadyState, RequestMethod, ResponseType } from "../enums";
import { ResponseOptions } from "../response_options";
import { isSuccess } from "../http_utils";


/**
 * Creates connections using `Nodejs Http Module`. Given a fully-qualified
 * request, an `NodeConnection` will immediately create an `http request` object and send the
 * request.
 */
export class NodeConnection implements Connection {
  readyState: ReadyState;
  request: Request;
  response: Observable<Response>;
  private nodeOptions: NodeRequestOpts;

  constructor(req: Request, baseResponseOptions?: ResponseOptions) {
    this.request = req;
    this.response = new Observable<Response>((responseObserver: Observer<Response>) => {
      let responseOptions = new ResponseOptions({});
      let reqs = NodeRequest(this.nodeOptions, res => {
        responseOptions.headers = res.headers;
        responseOptions.status = res.statusCode;
        responseOptions.statusText = res.statusMessage;
        responseOptions.url = res.url;

        // Receive data
        let body: any = "";
        res.on("data", chunk => {
          body += chunk;
        });

        // Merge response options
        if (!isNull(baseResponseOptions)) {
          responseOptions = baseResponseOptions.merge(responseOptions);
        }

        // Complete request
        res.on("end", () => {
          // Assign body
          responseOptions.body = body;
          let response = new Response(responseOptions);
          response.ok = isSuccess(res.statusCode);
          if (response.ok) {
            responseObserver.next(response);
            responseObserver.complete();
            return;
          }
          responseObserver.error(response);
        });
      });

      // Request error
      reqs.on("error", (err: any) => {
        let errResponseOptions = new ResponseOptions({
          body: err,
          status: responseOptions.status,
          statusText: responseOptions.statusText,
          type: ResponseType.Error,
        });
        if (!isNull(baseResponseOptions)) {
          errResponseOptions = baseResponseOptions.merge(errResponseOptions);
        }
        responseObserver.error(new Response(errResponseOptions));
      });

      // Send data
      reqs.write(req.getBody());
      // Close connection
      reqs.end();
    });
  }

  /**
   * 
   */
  private generateOptions(req: Request): void {
    // parser string url to node Url object
    const urlParsed: Url = parse(req.url);
    // assign url
    this.nodeOptions = assign({}, this.nodeOptions, urlParsed);
    this.nodeOptions.method = RequestMethod[req.method].toUpperCase();
    this.nodeOptions.headers = req.headers.toJSON();
  }
}

export class NodeBackend implements ConnectionBackend {
  constructor(private _baseResponseOptions: ResponseOptions) {
  }

  createConnection(request: Request): NodeConnection {
    return new NodeConnection(request, this._baseResponseOptions);
  }
}